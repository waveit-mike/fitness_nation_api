<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta name="robots" content="noindex, nofollow">
    <meta name="googlebot" content="noindex">
    <meta name="copyright" content="WaveIT Pitesti">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script
            src="https://code.jquery.com/jquery-3.4.1.min.js"
            integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
            crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
          integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <title>Adaugare Planuri Clienti</title>
</head>
<body>
<div class="container m-3">
    <div class="row">
        &nbsp;
    </div>
</div>
<div class="container mb-4">
    <div class="row mb-4"
         style="background-image: url('https://fitness-nation.ro/wp-content/uploads/2018/04/MOCKUP_23Mar18_1526_B50242_2-22.png'); background-color: #ffffff; background-position: center; background-repeat: no-repeat; -webkit-background-size: contain;background-size: contain; height: 100px; position: relative;">
    </div>
    <div class="row m-2 bg-white">
        &nbsp;
    </div>
    <div class="row justify-content-center mt-4 p-4 shadow-lg"
         style="background-image: linear-gradient(to right top, #13a89e, #009eac, #0093b8, #0085be, #0e76bc); border: 1px solid #13a89e; -webkit-border-radius: 8px;-moz-border-radius: 8px;border-radius: 8px;">
        <div class="row p-1">
            <h3 class="text-center text-white">Adauga Planuri /Antrenamente Clientilor</h3>
        </div>
        <div class="col-md-8 m-4 p-3 bg-white shadow-lg"
             style="-webkit-border-radius: 7px;-moz-border-radius: 7px;border-radius: 7px;">
            <form method="POST" action="{{URL::to('/') . '/api/admin/add/new-data/update-users-data/store-new-plan'}}"
                  enctype="multipart/form-data">
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <label for="email" class="col-form-label">Email-ul Clientului</label>
                        <input type="text" class="form-control" id="email" name="email"
                               value="" placeholder="Ex: marian@email.com">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="workouts">Antrenamente</label>
                        <select class="form-control" id="workouts" name="workout">
                            <option value="0" title="Nu se aplica" selected>Nu se aplica</option>
                            @foreach($workouts as $k => $workout)
                                <option value="{{$k . '_' . $workout}}" title="{{$workout}}">{{$workout}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="plans">Planuri</label>
                        <select class="form-control" id="plans" name="plan">
                            <option value="0" title="Nu se aplica" selected>Nu se aplica</option>
                            @foreach($plans as $k => $plan)
                                <option value="{{$k . '_' . $plan}}" title="{{$plan}}">{{$plan}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <button class="btn btn-danger" type="submit" id="submit" name="submit" title="Salveaza Datele">
                    Salveaza
                </button>
            </form>
        </div>
        <div class="col-md-8 m-4 p-3 bg-white shadow-lg"
             style="-webkit-border-radius: 7px;-moz-border-radius: 7px;border-radius: 7px;">
            <div class="table-responsive" style="height: 360px; max-height: 360px;">
                <table class="table table-hover table-sm">
                    <thead>
                    <tr>
                        <th scope="col" class="text-center align-middle">Id Plan/ Antrenament</th>
                        <th scope="col" class="text-center align-middle">Nume Plan/ Antrenament</th>
                        <th scope="col" class="text-center align-middle">Email Client</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($userPlans as $userPlan)
                        <tr>
                            <th scope="row" class="text-center align-middle">{{$userPlan->data_id}}</th>
                            <td class="text-center align-middle">{{$userPlan->data_name}}</td>
                            <td class="text-center align-middle">{{$userPlan->user_email}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $("form").submit(function (e) {
            e.preventDefault();
        });
        $('#submit').click(function () {

            if ($('select[name=workout]').val() != "0" && $('select[name=plan]').val() != "0") {
                alert("Alegeti pe rand fie un Antrenament fie un Plan. Nu se pot introduce simultan datele!");
            } else if (($("#email").val() == "")) {
                alert("Inroduceti o adresa de email!");
            } else if ($('select[name=workout]').val() == "0" && $('select[name=plan]').val() == "0") {
                alert("Alegeti un Antrenament sau un Plan!");
            } else {
                $("form").unbind("submit").submit();
            }
        });
    });
</script>
</body>
</html>