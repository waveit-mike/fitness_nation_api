<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Fitness Nation - Resetare Parola</title>
</head>
<body>
<table border="0" align="center"
       style="width: 700px; max-width: 700px; margin: 0 auto; text-align: left; background-image: linear-gradient(to right top, #13a89e, #009eac, #0093b8, #0085be, #0e76bc); border: 1px solid #009eac; border-radius: 7px;">
    <tr>
        <td align="center" style="width: 530px; padding: 50px 5px 50px 5px; margin: 0 auto;">
            <table border="0" align="center"
                   style="width: 530px; max-width: 530px; margin: 0 auto; background-color: #fff; -webkit-box-shadow: 10px 25px 31px -2px rgba(0,0,0,0.36);
-moz-box-shadow: 10px 25px 31px -2px rgba(0,0,0,0.36);
box-shadow: 10px 25px 31px -2px rgba(0,0,0,0.36); border-radius: 5px;">
                <tr>
                    <td align="left"
                        style="width: 520px; background-color: #fff; text-align: left; padding: 5px;">
                        <p style="color: #333; float: left; font-size: 18px;">Resetare Parola Cont Fitness Nation</p>
                        <a class="nav-link" href="https://fitness-nation.ro"><img
                                    src="https://fitness-nation.ro/wp-content/uploads/2018/04/MOCKUP_23Mar18_1526_B50242_2-22.png"
                                    alt="Fitness Nation"
                                    style="width: 150px; float: right; margin: 0 auto; display: inline;"></a>
                    </td>
                </tr>
                <tr>
                    <td align="center"
                        style="width: 520px; margin: 0 auto; background-color: #fff; padding: 5px;">
                        <p style="color: #333; text-align: left; text-transform: capitalize;">
                            Salut {{$user->full_name}},</p>
                        <br><br>
                        <p style="color: #333; text-align: left;">
                            Am primit o solicitare pentru resetarea parolei asociata contului tau. Daca nu ai facut tu
                            aceasta solicitare, te rugam sa ignori pur si simplu acest email. Daca solicitare este
                            facuta de tine, iti poti reseta parola utilizand link-ul de mai jos:
                        </p>
                        <br>
                        <a href="{{URL::to('/') . '/api/user/reset-password/'. $user->id}}"
                           style="padding: 7px; background-color: #c51f1a; border: 1px solid #c51f1a; -webkit-border-radius: 10px;-moz-border-radius: 10px;border-radius: 10px; color: #FFF; font-size: 15px; text-decoration: none; text-align: center !important; margin: 0 auto;"
                           title="Confirma Comanda">Click aici pentru a iti reseta parola</a>
                        <br>
                        <br>
                    </td>
                </tr>
                <tr>
                    <td align="center"
                        style="width: 100%; height: 10px; line-height: 10px; border-bottom: 1px dotted #333">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="center"
                        style="min-width: 530px; width: 100%; max-width: 530px !important; margin: 0 auto; background-color: #fff; text-align: left; padding: 10px;">
                        Iti multumim, <br/>
                        Fitness Nation.
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>
