<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="robots" content="noindex, nofollow">
    <meta name="googlebot" content="noindex">
    <meta name="copyright" content="WaveIT Pitesti">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <title>Resetare Parola</title>
</head>
<body>
<div class="container m-3">
    <div class="row">
        &nbsp;
    </div>
</div>
<div class="container">
    <div class="row mb-4"
         style="background-image: url('https://fitness-nation.ro/wp-content/uploads/2018/04/MOCKUP_23Mar18_1526_B50242_2-22.png'); background-color: #ffffff; background-position: center; background-repeat: no-repeat; -webkit-background-size: contain;background-size: contain; height: 200px; position: relative;">
    </div>
    <div class="row m-2 bg-white">
        &nbsp;
    </div>
    <div class="row justify-content-center mt-4 p-4 shadow-lg"
         style="background-image: linear-gradient(to right top, #13a89e, #009eac, #0093b8, #0085be, #0e76bc); border: 1px solid #13a89e; -webkit-border-radius: 8px;-moz-border-radius: 8px;border-radius: 8px;">
        <div class="col-md-8 m-4 p-3 bg-white shadow-lg"
             style="-webkit-border-radius: 7px;-moz-border-radius: 7px;border-radius: 7px;">
            <form method="POST" action="{{URL::to('/') . '/api/user/update-password/'. $user->id}}}"
                  enctype="multipart/form-data">
                <div class="form-group row">
                    <label for="email" class="col-sm-2 col-form-label">Email:</label>
                    <div class="col-sm-7">
                        <input type="text" readonly class="form-control-plaintext" id="email"
                               value="{{$user->email}}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="password" class="col-sm-2 col-form-label">Parola:</label>
                    <div class="col-sm-7">
                        <input type="password" class="form-control" id="password" name="password"
                               placeholder="Introdu aici noua ta parola">
                    </div>
                </div>
                <button class="btn btn-danger" type="submit" title="Reseteaza Parola">Reseteaza Parola</button>
            </form>
        </div>
    </div>
</div>

</body>
</html>