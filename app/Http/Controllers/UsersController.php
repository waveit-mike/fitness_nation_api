<?php

namespace App\Http\Controllers;

use App;
use App\Models\User;
use App\Models\UserProfile;
use App\Models\UserPlan;
use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Mail;
use App\Mail\ResetPassword;
use App\Models\UserDimension;


class UsersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    protected $token = "token";

    public function __construct()
    {
        //
    }

    //

    public function getAllUsers()
    {
        $user = User::with('profile')->get();

        return response()->json($user);
    }

    public function createNewUser(Request $request)
    {

        $message = "";
        $emailExist = false;
        $newUser = "";
        $newUserId = "";
        $userProfileImage = "";
        $tokenKeyExists = false;
        $token = false;
        $req = $request->all();
        $status = false;
        $defaultProfileImage = "https://fitness-nation.ro/wp-content/uploads/2019/03/profile-default.gif";

        if (array_key_exists('token', $req)) {
            $tokenKeyExists = true;
            if ($req['token'] == "token") {
                $token = true;
                if (isset($req['name'])) {
                    $requestName = $req['name'];
                } else {
                    $requestName = null;
                }
                $requestEmail = $req['email'];
                $options = [
                    'cost' => 12,
                ];
                $requestPassword = password_hash($req['password'], PASSWORD_BCRYPT, $options);
                $getUsers = User::where('email', $requestEmail)->count();
                if ($getUsers > 0) {
                    $message = "Un alt cont este asociat acestei adrese de email.";
                    $emailExist = true;
                } else {
                    if ($emailExist === false) {
                        $data = [
                            'full_name' => $requestName,
                            'email' => $requestEmail,
                            'password' => $requestPassword,
                            'profile_image' => $defaultProfileImage,
                        ];
                        $newUser = User::create($data);
                        $newUserId = $newUser->id;
                        $userProfileImage = $newUser->profile_image;
                        $message = "Contul a fost creat cu succes!";
                        $status = true;
                    }
                }
            } else {

                $message .= "Invalid Token. " . " ";

            }
        } else {

            $message .= "Token not found in sent Request. ";

        }


        return response()->json([
            'status' => $status,
            'emailExist' => $emailExist,
            'message' => $message,
            'newUserId' => $newUserId,
            'profileImage' => $userProfileImage,
        ]);

    }

    public function userUpdateProfileImage(Request $request)
    {

        $message = "";
        $userCheck = false;
        $newImage = "";
        $updatedProfileImage = null;
        $req = $request->all();
        $update = false;
        if (isset($req['token'])) {
            $token = $req['token'];

            if ($token == "token") {

                if (isset($req['id'])) {

                    if (User::find($req['id']) != null) {

                        $userCheck = true;
                        $getUser = User::find($req['id']);

                        if (isset($req['profileImage']) && $req['profileImage'] != null) {
                            $newImage = $req['profileImage'];
                            $getUser->profile_image = $newImage;
                            $getUser->save();
                            $updatedProfileImage = $getUser->profile_image;
                            $update = true;

                            $message = "Imaginea a fost actualizata.";

                        } else {

                            $message .= "Imaginea nu a fost gasit in solicitare Sau valoarea parametrului nu este setata.";

                        }

                    } else {

                        $message .= "Nu a fost gasit niciun cont cu acest ID" . " | ";

                    }

                } else {

                    $message = "ID nu a fost gasit in solicitarea facuta.";
                }

            } else {

                $message .= "Invalid Token. " . " ";

            }

        } else {

            $message .= "Token not found in sent Request. ";

        }

        return response()->json([
            'updatedImage' => $updatedProfileImage,
            'update' => $update,
            'message' => $message,
            'userCheck' => $userCheck,
        ]);

    }

    public
    function userLogin(Request $request)
    {
        $tokenKeyExists = false;
        $token = false;
        $message = "";
        $success = false;
        $userName = false;
        $defaultProfileImage = "";
        $userId = 0;
        $req = $request->all();
        if (array_key_exists('token', $req)) {
            $tokenKeyExists = true;
            if ($req['token'] == "token") {
                $requestEmail = $req['email'];
                $requestPassword = $req['password'];
                $hashedPassword = $requestPassword;

                $getUserData = User::where('email', $requestEmail)->select('id', 'full_name', 'password', 'profile_image')->get();

                if (count($getUserData) > 0) {

                    $dbPassword = $getUserData[0]['password'];

                    if (password_verify($requestPassword, $dbPassword)) {

                        $success = true;
                        $userName = (string)$getUserData[0]['full_name'];
                        $userId = $getUserData[0]['id'];
                        $defaultProfileImage = $getUserData[0]['profile_image'];
                        $message = "V-ati autentificat cu succes.";

                    } else {

                        $message .= 'Parola introdusa este incorecta.';
                    }
                } else {

                    $message .= 'Nu a fost gasit niciun cont cu aceasta adresa de email.';
                }

            } else {

                $message .= "Invalid Token. " . " ";

            }
        } else {

            $message .= "Token not found in sent Request. ";

        }

        return response()->json([
            'status' => $success,
            'message' => $message,
            'UserFullName' => $userName,
            'userId' => $userId,
            'profileImage' => $defaultProfileImage,
        ]);
    }

    public
    function resetPasswordEmail(Request $request)
    {

        $sendResetPasswordEmail = false;
        $message = "";
        $req = $request->all();

        if (isset($req['token'])) {
            $token = $req['token'];
            if ($token == "token") {
                $email = $req['email'];
                $getUser = User::where('email', $email)->select('id', 'full_name', 'email')->get();

                if (count($getUser) > 0) {

                    $user = $getUser[0];
                    Mail::to((string)$email)->send(new ResetPassword($user));
                    $sendResetPasswordEmail = true;
                    $message = 'A fost trimis un email pentru resetarea parolei.';

                } else {

                    $message .= 'Nu a fost gasit niciun cont cu aceasta adresa de email.';

                }

            } else {

                $message .= "Invalid Token";

            }
        } else {

            $message .= "Token not found in sent Request";

        }

        return response()->json([
            'message' => $message,
            'resetPasswordEmailSendStatus' => $sendResetPasswordEmail,
        ]);

    }

    public
    function resetPasswordView($id)
    {

        $user = User::find($id);

        return view('reset-password', compact('user'));
    }

    public
    function updatePassword(Request $request, $id)
    {

        $user = User::find($id);
        $options = [
            'cost' => 12,
        ];
        $password = $request->input('password');

        $newPassword = password_hash($password, PASSWORD_BCRYPT, $options);

        $user->password = $newPassword;
        $user->save();

        return view('reset-password-success');

    }

    public
    function updateUserProfileData(Request $request)
    {
        $createProfileData = false;
        $message = "";
        $userCheck = false;
        $weightData = false;
        $dateData = false;
        $req = $request->all();

        if (isset($req['token'])) {
            $token = $req['token'];
            if ($token == "token") {

                if (isset($req['id'])) {

                    if (User::find($req['id']) != null) {

                        if (isset($req['weight']) && $req['weight'] != null) {

                            $weightData = true;

                            if (isset($req['date']) && $req['date'] != null) {

                                $dateData = true;

                                $userCheck = true;

                                $id = $req['id'];
                                $weight = $req['weight'];
                                $date = $req['date'];
                                $data = [
                                    'user_id' => $id,
                                    'weight' => $weight,
                                    'date' => $date,
                                ];

                                UserProfile::create($data);

                                $createProfileData = true;

                                $message = "Datele au fost salvate";

                            } else {

                                $message .= "Date nu a fost gasit in solicitarea trimisa Sau valoarea parametrului nu este setata..";

                            }

                        } else {

                            $message .= "Weight nu a fost gasit in solicitare Sau valoarea parametrului nu este setata.";

                        }

                    } else {

                        $message .= "Nu a fost gasit niciun cont cu acest ID" . " | ";

                    }

                } else {

                    $message = "ID nu a fost gasit in solicitarea facuta.";
                }

            } else {

                $message .= "Invalid Token. " . " ";

            }
        } else {

            $message .= "Token not found in sent Request. ";

        }

        return response()->json([
            'create_Profile_Data' => $createProfileData,
            'user_Check_If_Exists' => $userCheck,
            'weight_Found_In_Request' => $weightData,
            'date_Found_In_Request' => $dateData,
            'message' => $message,
        ]);
    }

    public
    function getUserProfileData(Request $request)
    {
        $message = "";
        $userCheck = false;
        $req = $request->all();
        $filteredData = null;

        if (isset($req['token'])) {

            $token = $req['token'];

            if ($token == "token") {

                if (isset($req['id'])) {

                    if (User::find($req['id']) != null) {

                        $userCheck = true;
                        $id = $req['id'];

                        $requestedUserData = User::with('profile')->find($id);
                        $filteredData = $requestedUserData->profile;

                        $message = "Datele contului solicitat au fost gasite";

                    } else {

                        $message .= "Nu a fost gasit niciun cont cu acest ID.";

                    }

                } else {

                    $message .= "ID nu a fost gasit in solicitarea facuta.";

                }

            } else {

                $message .= "Invalid Token. " . " ";

            }


        } else {

            $message .= "Token not found in sent Request. ";

        }

        return response()->json([
            'user_Check_If_Exists' => $userCheck,
            'user_Data' => $filteredData,
            'message' => $message,
        ]);
    }

    public
    function destroyUserWeight(Request $request)
    {

        $message = "";
        $userCheck = false;
        $req = $request->all();
        $destroyStatus = false;

        if (isset($req['token'])) {

            $token = $req['token'];

            if ($token == "token") {

                if (isset($req['id'])) {

                    if (User::find($req['user_id']) != null) {

                        $userCheck = true;
                        $id = $req['id'];

                        if (UserProfile::destroy($id)) {
                            $destroyStatus = true;
                            $message = "Inregistrarea a fost stearsa";

                        } else {
                            $message = "Inregistrarea nu a putut fi stearsa. Incercati mai tarziu.";
                        }

                    } else {

                        $message .= "Nu a fost gasit niciun cont cu acest ID.";

                    }

                } else {

                    $message .= "ID-ul iregistrarii nu a fost gasit in solicitarea facuta.";

                }

            } else {

                $message .= "Invalid Token. " . " ";

            }


        } else {

            $message .= "Token not found in sent Request. ";

        }

        return response()->json([
            'destroy' => $destroyStatus,
            'user_Check_If_Exists' => $userCheck,
            'message' => $message,
        ]);

    }

    public function storeUserDimensions(Request $request)
    {
        $arms = "";
        $chest = "";
        $thighs = "";
        $legs = "";
        $waist = "";
        $forearm = "";
        $dateData = "";
        $userDimensions = [];
        $userCheck = false;
        $message = "";
        $req = $request->all();

        if (isset($req['token'])) {

            $token = $req['token'];

            if ($token == "token") {

                if (isset($req['id'])) {

                    if (User::find($req['id']) != null) {

                        $userCheck = true;

                        if (isset($req['arms']) && $req['arms'] != null) {
                            $arms = $req['arms'];
                        } else {
                            $arms = null;
                        }

                        if (isset($req['chest']) && $req['chest'] != null) {
                            $chest = $req['chest'];
                        } else {
                            $chest = null;
                        }

                        if (isset($req['thighs']) && $req['thighs'] != null) {
                            $thighs = $req['thighs'];
                        } else {
                            $thighs = null;
                        }

                        if (isset($req['legs']) && $req['legs'] != null) {
                            $legs = $req['legs'];
                        } else {
                            $legs = null;
                        }

                        if (isset($req['waist']) && $req['waist'] != null) {
                            $waist = $req['waist'];
                        } else {
                            $waist = null;
                        }

                        if (isset($req['forearm']) && $req['forearm'] != null) {
                            $forearm = $req['forearm'];
                        } else {
                            $forearm = null;
                        }


                        if (isset($req['date']) && $req['date'] != null) {

                            $dateData = $req['date'];
                            $id = $req['id'];
                            $data = [
                                'user_id' => $id,
                                'arms' => $arms,
                                'chest' => $chest,
                                'thighs' => $thighs,
                                'legs' => $legs,
                                'waist' => $waist,
                                'forearm' => $forearm,
                                'date' => $dateData,
                            ];

                            if ($savedData = UserDimension::create($data)) {

                                $message = "Dimensiunile au fost salvate";
                                $userDimensions = $savedData;

                            } else {
                                $message = "A aparut o problema la salvarea datelor. Va rugma sa incercati mai tarziu.";
                            }

                        } else {

                            $message .= "Date nu a fost gasit in solicitarea trimisa Sau valoarea parametrului nu este setata..";

                        }

                    } else {

                        $message .= "Nu a fost gasit niciun cont cu acest ID" . " | ";

                    }

                } else {

                    $message = "ID nu a fost gasit in solicitarea facuta.";
                }

            } else {

                $message .= "Invalid Token. " . " ";

            }
        } else {

            $message .= "Token not found in sent Request. ";

        }

        return response()->json([
            'newDimensionsData' => $userDimensions,
            'user_check' => $userCheck,
            'message' => $message,
        ]);
    }

    public function getUserDimensions(Request $request)
    {
        $message = "";
        $userCheck = false;
        $req = $request->all();
        $filteredData = [];

        if (isset($req['token'])) {

            $token = $req['token'];

            if ($token == "token") {

                if (isset($req['id'])) {

                    if (User::find($req['id']) != null) {

                        $userCheck = true;
                        $id = $req['id'];

                        $requestedUserData = User::with('dimensions')->find($id);
                        $filteredData = $requestedUserData->dimensions;
                        if (count($filteredData)) {
                            $message = "Dimensiunile contului solicitat au fost gasite";
                        } else {
                            $message = "Contul nu are inca setate dimensiuni.";
                        }
                    } else {

                        $message .= "Nu a fost gasit niciun cont cu acest ID.";

                    }

                } else {

                    $message .= "ID nu a fost gasit in solicitarea facuta.";

                }

            } else {

                $message .= "Invalid Token. " . " ";

            }


        } else {

            $message .= "Token not found in sent Request. ";

        }

        return response()->json([
            'user_Check_If_Exists' => $userCheck,
            'user_Data' => $filteredData,
            'message' => $message,
        ]);
    }

    public function destroyUserDimensions(Request $request)
    {
        $message = "";
        $userCheck = false;
        $req = $request->all();
        $destroyStatus = false;

        if (isset($req['token'])) {

            $token = $req['token'];

            if ($token == "token") {

                if (User::find($req['user_id']) != null) {
                    $userCheck = true;

                    if (isset($req['id'])) {

                        $id = $req['id'];

                        if (UserDimension::destroy($id)) {
                            $destroyStatus = true;
                            $message = "Inregistrarea a fost stearsa.";

                        } else {

                            $message = "Inregistrarea nu a putut fi stearsa. Incercati mai tarziu.";
                        }

                    } else {

                        $message .= "ID-ul iregistrarii nu a fost gasit in solicitarea facuta.";
                    }

                } else {

                    $message = "Nu a fost gasit niciun cont cu acest ID.";

                }

            } else {

                $message .= "Invalid Token. " . " ";

            }

        } else {

            $message .= "Token not found in sent Request. ";

        }

        return response()->json([
            'destroy' => $destroyStatus,
            'user_check' => $userCheck,
            'message' => $message,
        ]);
    }

    public
    function getPlansIndex()
    {

        $userPlans = UserPlan::all();

        $workouts = [
            3 => 'Program de slabire baieti',
            4 => 'Program de slabire fete',
            5 => 'Programul de masă musculară',
            6 => 'Colecție de Antrenamente pentru toate grupele',
            7 => 'Antrenamente pentru Piept + Bonus Abdomen',
            8 => 'Antrenamente pentru Brate',
            9 => 'Antrenamente pentru Spate + Bonus Trapez',
            10 => 'Antrenamente pentru Umeri',
            11 => 'Antrenamente pentru Picioare + Bonus Gambe'
        ];

        $plans = [
            100 => 'Plan 1300-1400 Kcal',
            101 => 'Plan 1400-1500 Kcal',
            102 => 'Plan 1500-1600 Kcal',
            103 => 'Plan 1600-1700 Kcal',
            104 => 'Plan 1700-1800 Kcal',
            105 => 'Plan 1800-1900 Kcal',
            106 => 'Plan 1900-2000 Kcal',
            107 => 'Plan 2000-2100 Kcal',
            108 => 'Plan 2100-2200 Kcal',
            109 => 'Plan 2200-2300 Kcal',
            110 => 'Plan 2300-2400 Kcal',
            111 => 'Plan 2400-2500 Kcal',
            112 => 'Plan 2500-2600 Kcal',
            113 => 'Plan 2600-2700 Kcal',
            114 => 'Plan 2700-2800 Kcal',
            115 => 'Plan 2800-2900 Kcal',
            116 => 'Plan 2900-3000 Kcal',
            117 => 'Plan 3000-3100 Kcal',
            118 => 'Plan 3100-3200 Kcal',
            119 => 'Plan 3200-3300 Kcal',
            120 => 'Plan 3300-3400 Kcal',
            121 => 'Plan 3400-3500 Kcal',
        ];

        return view('add-plans', compact('userPlans', 'workouts', 'plans'));
    }

    public function addNewPlan(Request $request)
    {
        $planId = "";
        $planName = "";
        $workoutId = "";
        $workoutName = "";
        $data = array();
        $isPlan = false;
        $isWorkout = false;
        $plan = $request->input('plan');
        $workout = $request->input('workout');

        if ($plan != "0") {
            $isPlan = true;
            $plan = explode("_", $plan);
            $planId = $plan[0];
            $planName = $plan[1];
//            var_dump("ID:" . $planId);
//            var_dump("NAME:" . $planName);
        }

        if ($workout != "0") {
            $isWorkout = true;
            $workout = explode("_", $workout);
            $workoutId = $workout[0];
            $workoutName = $workout[1];
        }

        if ($isPlan == true) {

            $data = [
                'data_id' => $planId,
                'data_name' => $planName,
                'user_email' => $request->input('email')
            ];

        } elseif ($isWorkout == true) {
            $data = [
                'data_id' => $workoutId,
                'data_name' => $workoutName,
                'user_email' => $request->input('email')
            ];
        }

        UserPlan::create($data);


        $userPlans = UserPlan::all();

        $workouts = [
            3 => 'Program de slabire baieti',
            4 => 'Program de slabire fete',
            5 => 'Programul de masă musculară',
            6 => 'Colecție de Antrenamente pentru toate grupele',
            7 => 'Antrenamente pentru Piept + Bonus Abdomen',
            8 => 'Antrenamente pentru Brate',
            9 => 'Antrenamente pentru Spate + Bonus Trapez',
            10 => 'Antrenamente pentru Umeri',
            11 => 'Antrenamente pentru Picioare + Bonus Gambe'
        ];

        $plans = [
            100 => 'Plan 1300-1400 Kcal',
            101 => 'Plan 1400-1500 Kcal',
            102 => 'Plan 1500-1600 Kcal',
            103 => 'Plan 1600-1700 Kcal',
            104 => 'Plan 1700-1800 Kcal',
            105 => 'Plan 1800-1900 Kcal',
            106 => 'Plan 1900-2000 Kcal',
            107 => 'Plan 2000-2100 Kcal',
            108 => 'Plan 2100-2200 Kcal',
            109 => 'Plan 2200-2300 Kcal',
            110 => 'Plan 2300-2400 Kcal',
            111 => 'Plan 2400-2500 Kcal',
            112 => 'Plan 2500-2600 Kcal',
            113 => 'Plan 2600-2700 Kcal',
            114 => 'Plan 2700-2800 Kcal',
            115 => 'Plan 2800-2900 Kcal',
            116 => 'Plan 2900-3000 Kcal',
            117 => 'Plan 3000-3100 Kcal',
            118 => 'Plan 3100-3200 Kcal',
            119 => 'Plan 3200-3300 Kcal',
            120 => 'Plan 3300-3400 Kcal',
            121 => 'Plan 3400-3500 Kcal',
        ];


        return view('add-plans', compact('userPlans', 'workouts', 'plans'));
    }

    public function mobileAPIGetUserPlans(Request $request)
    {
        $message = "";
        $userCheck = false;
        $req = $request->all();
        $userPlans = [];

        if (isset($req['token'])) {

            $token = $req['token'];

            if ($token == "token") {

                if (isset($req['email'])) {

                    if (UserPlan::where('user_email', $req['email'])->get() != null) {

                        $userCheck = true;
                        $email = $req['email'];

                        $userPlans = UserPlan::where('user_email', $email)->select('id', 'data_id', 'data_name', 'user_email')->get();

                    } else {

                        $message .= "Nu au fost gasite inregistrari pentru aceasta adresa de email.";

                    }

                } else {

                    $message .= "Email-ul nu a fost gasit in solicitarea facuta.";

                }

            } else {

                $message .= "Invalid Token. " . " ";

            }


        } else {

            $message .= "Token not found in sent Request. ";

        }

        return response()->json([
            'user_Check_If_Exists' => $userCheck,
            'user_Data' => $userPlans,
            'message' => $message,
        ]);
    }

}
