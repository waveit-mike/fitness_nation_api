<?php
/**
 * Created by PhpStorm.
 * User: Mike
 * Date: 22/03/2019
 * Time: 14:01
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class UserPlan extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = "users_plans";

    public $primaryKey = "id";

    public $timestamps = true;

    protected $fillable = [
        'data_id', 'data_name', 'user_email'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

//    public function user()
//    {
//        return $this->hasOne('App\Models\User', 'id', 'user_id');
//    }

}