<?php
/**
 * Created by PhpStorm.
 * User: Mike
 * Date: 22/03/2019
 * Time: 14:01
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class UserDimension extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = "user_dimensions";

    public $primaryKey = "id";

    public $timestamps = true;

    protected $fillable = [
        'user_id', 'arms', 'chest', 'thighs', 'legs', 'waist', 'forearm', 'date'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

    public function user()
    {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }
}