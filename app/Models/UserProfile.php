<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;


class UserProfile extends Model
{


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = "user_profile";

    public $primaryKey = "id";

    public $timestamps = true;

    protected $fillable = [
        'user_id', 'weight', 'date'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

    public function user()
    {

        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }

}
