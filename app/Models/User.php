<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;


class User extends Model
{
//    use Authenticatable, Authorizable;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = "users";

    public $primaryKey = "id";

    public $timestamps = true;

    protected $fillable = [
        'full_name', 'email', 'password', 'profile_image'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    public function profile()
    {
        return $this->hasMany('App\Models\UserProfile', 'user_id', 'id');
    }

    public function dimensions()
    {
        return $this->hasMany('App\Models\UserDimension', 'user_id', 'id');
    }
}
