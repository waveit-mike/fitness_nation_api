<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});


// Generate random 32 length key
$router->get('/key', function () {
    echo str_random(32);
});

$router->get('foo', function () {
    return 'Hello World';
});

$router->group(['prefix' => 'api'], function () use ($router) {

    // Get All Accounts
    // Method: GET
    // http://localhost:8080/api/user/get-all-users
    $router->get('user/get-all-users', 'UsersController@getAllUsers');

    // Create new Account
    // Method: POST
    // http://localhost:8080/api/user/create-user?name=Mike Mikeloss&email=mike@waveit.ro&password=test&token=token
    $router->post('user/create-user', 'UsersController@createNewUser');

    // Update User Profile Image
    // Method: POST
    //http://localhost:8080/api/user/update-profile-image?token=token&id=3&profileImage=https://en.gravatar.com/userimage/141984024/6c418b7a408f3d1487509ec1a7756d8e.jpeg
    $router->post('user/update-profile-image', 'UsersController@userUpdateProfileImage');

    // User Login
    // Method: GET
    // http://localhost:8080/api/user/user-login?email=mike@waveit.ro&token=token&password=test
    $router->get('user/user-login/', 'UsersController@userLogin');

    // Reset Password Email
    // Method: GET
    // http://localhost:8080/api/user/reset-password-email?email=mike@waveit.ro&token=token
    $router->get('/user/reset-password-email', 'UsersController@resetPasswordEmail');

    // Reset Password Route to Browser View
    $router->get('/user/reset-password/{id}', 'UsersController@resetPasswordView');

    // Reset Password Route
    $router->post('/user/update-password/{id}', 'UsersController@updatePassword');

    // Create User Profile Data(weight, date)
    // Method: POST
    // http://localhost:8080/api/user/save-weight-data?id=1&token=token&weight=61&date=15.02.2018
    $router->post('/user/save-weight-data', 'UsersController@updateUserProfileData');

    // Get USer Profile Data(weight, date)
    // Method: GET
    // http://localhost:8080/api/user/get-profile-data?token=token&id=1
    $router->get('/user/get-profile-data', 'UsersController@getUserProfileData');

    // Destroy User Data(weight, date)
    // Method: DELETE
    // http://localhost:8080/api/user/destroy-profile-data?token=token&id=1&user_id=1
    $router->delete('/user/destroy-profile-data', 'UsersController@destroyUserWeight');

    // Save User Dimensions Data(arms, chest, legs, etc)
    // Method: Post
    // http://localhost:8080/api/user/save-dimensions-data?token=token&id=3&date=22.03.2019&arms=22&chest=40&thighs=25&legs=55&waist=21&forearm=14
    $router->post('/user/save-dimensions-data', 'UsersController@storeUserDimensions');

    // Get User All Dimensions
    // Method: GET
    //http://localhost:8080/api/user/get-all-dimensions-data?token=token&id=3
    $router->get('/user/get-all-dimensions-data', 'UsersController@getUserDimensions');

    // Destroy User Dimensions Data Row (arms, chest, legs, etc)
    // Method: DELETE
    // http://localhost:8080/api/user/destroy-dimension-data?token=token&user_id=35&id=1
    $router->delete('/user/destroy-dimension-data', 'UsersController@destroyUserDimensions');

    $router->get('/68zFmjRpwxnR/admin/add/new-data/update-users-data/save-plans/', 'UsersController@getPlansIndex');

    $router->post('/admin/add/new-data/update-users-data/store-new-plan', 'UsersController@addNewPlan');

    // Get User Plans And Workouts by EMAIL
    // Method: GET
    // http://localhost:8080/api/user/get-plans-and-workouts?token=token&email=andreimiketanase@gmail.com
    $router->get('/user/get-plans-and-workouts', 'UsersController@mobileAPIGetUserPlans');

});