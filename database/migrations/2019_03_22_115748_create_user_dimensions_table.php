<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserDimensionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_dimensions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable()->default(0);
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('arms')->nullable()->default(null);
            $table->integer('chest')->nullable()->default(null);
            $table->integer('thighs')->nullable()->default(null);
            $table->integer('legs')->nullable()->default(null);
            $table->integer('waist')->nullable()->default(null);
            $table->integer('forearm')->nullable()->default(null);
            $table->string('date')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_dimensions');
    }
}
